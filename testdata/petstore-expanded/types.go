package petstore_expanded

import (
	"net/http"
)

type Error struct {
	Code    int32
	Message string
}
type NewPet struct {
	Name string
	Tag  string
	Id   int64
}
type Pet struct {
	Name string
	Tag  string
	Id   int64
}

type AddPetHandler struct {
	Request struct {
		NewPet *NewPet `json:"new_pet"`
	}
	Responses struct {
		OK struct {
			Pet *Pet `json:"pet"`
		} `status:"200"`
		Default struct {
			Error *Error `json:"error"`
		} `status:"0"`
	}
}

func (r *AddPetHandler) ServeHTTP(http.ResponseWriter, *http.Request) { panic("not implemented") }

type DeletePetHandler struct {
	Request struct {
		Id int64 `path:"id"`
	}
	Responses struct {
		NoContent struct{} `status:"204"`
		Default   struct {
			Error *Error `json:"error"`
		} `status:"0"`
	}
}

func (r *DeletePetHandler) ServeHTTP(http.ResponseWriter, *http.Request) { panic("not implemented") }

type FindPetByIdHandler struct {
	Request struct {
		Id int64 `path:"id"`
	}
	Responses struct {
		OK struct {
			Pet *Pet `json:"pet"`
		} `status:"200"`
		Default struct {
			Error *Error `json:"error"`
		} `status:"0"`
	}
}

func (r *FindPetByIdHandler) ServeHTTP(http.ResponseWriter, *http.Request) { panic("not implemented") }

type FindPetsHandler struct {
	Request struct {
		Tags  []string `query:"tags"`
		Limit int32    `query:"limit"`
	}
	Responses struct {
		OK      struct{} `status:"200"`
		Default struct {
			Error *Error `json:"error"`
		} `status:"0"`
	}
}

func (r *FindPetsHandler) ServeHTTP(http.ResponseWriter, *http.Request) { panic("not implemented") }
