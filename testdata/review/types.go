package review

import (
	"net/http"
	"time"
)

const (
	BreachProbabilityHigh         BreachProbability = 3
	BreachProbabilityMedium       BreachProbability = 2
	BreachProbabilityNone         BreachProbability = 1
	BreachProbabilityUnreviewable BreachProbability = 0

	BreachSeverityBreach BreachSeverity = 2
	BreachSeverityEvent  BreachSeverity = 0
	BreachSeverityRisk   BreachSeverity = 1

	SessionStatusError    SessionStatus = "error"
	SessionStatusQueued   SessionStatus = "untagged"
	SessionStatusReviewed SessionStatus = "tagged"
	SessionStatusStarted  SessionStatus = "in_progress"

	SourceAi    Source = "ml"
	SourceHuman Source = "hil"

	StatusCodeCorruptedDataFromClient      StatusCode = "CorruptedDataFromClient"
	StatusCodeExamTooLongException         StatusCode = "ExamTooLongException"
	StatusCodeException                    StatusCode = "Exception"
	StatusCodeInternalServerError          StatusCode = "InternalServerError"
	StatusCodeMLJobCompletedException      StatusCode = "MLJobCompletedException"
	StatusCodeMLJobInProgressException     StatusCode = "MLJobInProgressException"
	StatusCodeNoChunkInManifestException   StatusCode = "NoChunkInManifestException"
	StatusCodeVendorResponseException      StatusCode = "VendorResponseException"
	StatusCodeVideoChannelMissingException StatusCode = "VideoChannelMissingException"
)

type Breach struct {
	Name        string         `bson:"name" json:"name"`
	Description string         `bson:"description" json:"description"`
	Severity    BreachSeverity `bson:"severity" json:"severity"`
}
type BreachDictionary struct {
	Name  string    `bson:"name" json:"name"`
	Items []*Breach `bson:"items" json:"items"`
}
type BreachProbability int
type BreachSeverity int
type Count struct {
	Count int64 `json:"count"`
}
type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
type Incident struct {
	Start  int64   `bson:"start" json:"start"`
	End    int64   `bson:"end" json:"end"`
	Breach *Breach `bson:"breach" json:"breach"`
	Source Source  `bson:"source" json:"source"`
}
type IscReport struct {
	Id                    primitive.ObjectID `bson:"_id" json:"_id"`
	SessionId             string             `bson:"session_id" json:"session_id"`
	Vid                   string             `bson:"vid" json:"vid"`
	GenderPreference      bool               `bson:"gender_preference" json:"gender_preference"`
	FinalReport           *IscReportData     `bson:"final_report" json:"final_report"`
	TaggingStatus         string             `bson:"tagging_status" json:"tagging_status"`
	TaggingStartedAt      time.Time          `bson:"tagging_started_at" json:"tagging_started_at"`
	TaggingEndedAt        time.Time          `bson:"tagging_ended_at" json:"tagging_ended_at"`
	TaggerId              string             `bson:"tagger_id" json:"tagger_id"`
	CreatedAt             time.Time          `bson:"created_at" json:"created_at"`
	VideoDuration         int64              `bson:"video_duration" json:"video_duration"`
	MlStatus              string             `bson:"ml_status" json:"ml_status"`
	MlStartedAt           time.Time          `bson:"ml_started_at" json:"ml_started_at"`
	MlEndedAt             time.Time          `bson:"ml_ended_at" json:"ml_ended_at"`
	Retagged              bool               `bson:"retagged" json:"retagged"`
	EsSentAt              time.Time          `bson:"es_sent_at" json:"es_sent_at"`
	EsAiSentAt            time.Time          `bson:"es_ai_sent_at" json:"es_ai_sent_at"`
	EsHilSentAt           time.Time          `bson:"es_hil_sent_at" json:"es_hil_sent_at"`
	HumanProctoringNeeded bool               `bson:"human_proctoring" json:"human_proctoring"`
	AiProctoringRequired  bool               `bson:"ai_proctoring_re" json:"ai_proctoring_re"`
	HilAidNeeded          bool               `bson:"hil_aid_needed" json:"hil_aid_needed"`
	GroupAllowed          bool               `bson:"group_allowed" json:"group_allowed"`
	ResourceAllowed       bool               `bson:"resource_allowed" json:"resource_allowed"`
	LastModifiedAt        time.Time          `bson:"last_modified_at" json:"last_modified_at"`
	Error                 []string           `bson:"error" json:"error"`
	HilAutoSkip           bool               `bson:"hil_auto_skip" json:"hil_auto_skip"`
	Tag                   string             `bson:"tag" json:"tag"`
	Client                string             `bson:"client" json:"client"`
	Compliance            bool               `bson:"compliance" json:"compliance"`
	Environment           string             `bson:"environment" json:"environment"`
	IsDeleted             bool               `bson:"is_deleted" json:"is_deleted"`
	DeletedAt             time.Time          `bson:"deleted_at" json:"deleted_at"`
	DeletedComment        string             `bson:"deleted_comment" json:"deleted_comment"`
}
type IscReportData struct {
	Graph         ReportGraph `bson:"graph" json:"graph"`
	VideoDuration int64       `bson:"video_duration" json:"video_duration"`
	Info          ReportInfo  `bson:"info" json:"info"`
}
type IscTagger struct {
	Id        primitive.ObjectID `bson:"_id" json:"_id"`
	FirstName string             `bson:"first_name" json:"first_name"`
	LastName  string             `bson:"last_name" json:"last_name"`
	Email     string             `bson:"email" json:"email"`
	Password  string             `bson:"password" json:"-"`
	Gender    string             `bson:"gender" json:"gender"`
	Clients   []string           `bson:"clients" json:"clients"`
	UserType  string             `bson:"user_type" json:"user_type"`
}
type ProcwiseExamRules struct {
	AllowWebsites               bool     `json:"allow_websites"`
	AllowedWebsites             []string `json:"allowed_websites"`
	AllowExternalApplications   bool     `json:"allow_external_applications"`
	AllowedExternalApplications []string `json:"allowed_external_applications"`
	AllowTextbooks              bool     `json:"allow_textbooks"`
	AllowedTextbooks            []string `json:"allowed_textbooks"`
	AllowCalculator             bool     `json:"allow_calculator"`
	AllowPenPaper               bool     `json:"allow_pen_paper"`
	AllowAdditional             bool     `json:"allow_additional"`
	AllowedAdditional           []string `json:"allowed_additional"`
}
type ProcwiseMetadata struct {
	Token            string               `json:"token"`
	Institute        string               `json:"institute"`
	InstituteId      int                  `json:"institute_id"`
	Exam             string               `json:"exam"`
	ExamId           int                  `json:"exam_id"`
	StartTime        time.Time            `json:"start_time"`
	EndTime          time.Time            `json:"end_time"`
	StreamTypes      []string             `json:"stream_types"`
	ExamRules        *ProcwiseExamRules   `json:"exam_rules"`
	ExamInstructions string               `json:"exam_instructions"`
	Recordings       []*ProcwiseRecording `json:"recordings"`
}
type ProcwiseRecording struct {
	StreamType string `json:"stream_type"`
	Url        string `json:"url"`
}
type ProcwiseReport struct {
	Untagged        bool                `json:"untagged"`
	Retagged        bool                `json:"retagged"`
	SessionId       string              `json:"session_id"`
	GroupAllowed    bool                `json:"group_allowed"`
	ResourceAllowed bool                `json:"resource_allowed"`
	StatusCode      string              `json:"status_code"`
	Message         string              `json:"message"`
	Data            *ProcwiseReportData `json:"data"`
}
type ProcwiseReportData struct {
	Graph         ReportGraph
	VideoDuration int64 `json:"video_duration"`
	Info          ReportInfo
}
type ProcwiseStatus struct {
	Source            Source        `json:"source"`
	Status            SessionStatus `json:"status"`
	StatusDescription string        `json:"status_description"`
	StatusCode        StatusCode    `json:"status_code"`
}
type ReportGraph map[string]interface{}

func (r ReportGraph) Get(k string) (v []*ReportGraphIncident) {
	v, _ = r[k].([]*ReportGraphIncident)
	return
}
func (r *ReportGraph) Set(k string, v []*ReportGraphIncident)       { (*r)[k] = v }
func (r *ReportGraph) SetApplicantMissing(v []*ReportGraphIncident) { (*r)["applicant_missing"] = v }
func (r ReportGraph) ApplicantMissing() (v []*ReportGraphIncident) {
	v, _ = r["applicant_missing"].([]*ReportGraphIncident)
	return
}
func (r ReportGraph) HandGesture() (v []*ReportGraphIncident) {
	v, _ = r["hand_gesture"].([]*ReportGraphIncident)
	return
}
func (r *ReportGraph) SetHandGesture(v []*ReportGraphIncident) { (*r)["hand_gesture"] = v }
func (r ReportGraph) HeadMovement() (v []*ReportGraphIncident) {
	v, _ = r["head_movement"].([]*ReportGraphIncident)
	return
}
func (r *ReportGraph) SetHeadMovement(v []*ReportGraphIncident) { (*r)["head_movement"] = v }
func (r ReportGraph) MultiplePeople() (v []*ReportGraphIncident) {
	v, _ = r["multiple_people"].([]*ReportGraphIncident)
	return
}
func (r *ReportGraph) SetMultiplePeople(v []*ReportGraphIncident)    { (*r)["multiple_people"] = v }
func (r *ReportGraph) SetSuspiciousObjects(v []*ReportGraphIncident) { (*r)["suspicious_objects"] = v }
func (r ReportGraph) SuspiciousObjects() (v []*ReportGraphIncident) {
	v, _ = r["suspicious_objects"].([]*ReportGraphIncident)
	return
}
func (r *ReportGraph) SetVoiceActivity(v []*ReportGraphIncident) { (*r)["voice_activity"] = v }
func (r ReportGraph) VoiceActivity() (v []*ReportGraphIncident) {
	v, _ = r["voice_activity"].([]*ReportGraphIncident)
	return
}

type ReportGraphIncident struct {
	Start      int64  `bson:"start" json:"start"`
	End        int64  `bson:"end" json:"end"`
	Level      int    `bson:"level" json:"level"`
	Text       string `bson:"text" json:"text"`
	EventTitle string `bson:"event_title" json:"event_title"`
	Priority   int64  `bson:"priority" json:"priority"`
	From       string `bson:"from" json:"from"`
	Sf         string `bson:"sf" json:"sf"`
}
type ReportInfo map[string]interface{}

func (r ReportInfo) Get(k string) (v *ReportInfoIncident) { v, _ = r[k].(*ReportInfoIncident); return }
func (r *ReportInfo) Set(k string, v *ReportInfoIncident) { (*r)[k] = v }
func (r ReportInfo) ApplicantMissing() (v *ReportInfoIncident) {
	v, _ = r["applicant_missing"].(*ReportInfoIncident)
	return
}
func (r *ReportInfo) SetApplicantMissing(v *ReportInfoIncident)  { (*r)["applicant_missing"] = v }
func (r ReportInfo) CheatingStatus() (v bool)                    { v, _ = r["cheating_status"].(bool); return }
func (r *ReportInfo) SetCheatingStatus(v bool)                   { (*r)["cheating_status"] = v }
func (r ReportInfo) Duration() (v int)                           { v, _ = r["duration"].(int); return }
func (r *ReportInfo) SetDuration(v int)                          { (*r)["duration"] = v }
func (r *ReportInfo) SetDurationOffscreen(v *ReportInfoIncident) { (*r)["duration_offscreen"] = v }
func (r ReportInfo) DurationOffscreen() (v *ReportInfoIncident) {
	v, _ = r["duration_offscreen"].(*ReportInfoIncident)
	return
}
func (r *ReportInfo) SetDurationSpeaking(v *ReportInfoIncident) { (*r)["duration_speaking"] = v }
func (r ReportInfo) DurationSpeaking() (v *ReportInfoIncident) {
	v, _ = r["duration_speaking"].(*ReportInfoIncident)
	return
}
func (r *ReportInfo) SetHandGesture(v *ReportInfoIncident) { (*r)["hand_gesture"] = v }
func (r ReportInfo) HandGesture() (v *ReportInfoIncident) {
	v, _ = r["hand_gesture"].(*ReportInfoIncident)
	return
}
func (r *ReportInfo) SetHilTagged(v bool) { (*r)["hil_tagged"] = v }
func (r ReportInfo) HilTagged() (v bool)  { v, _ = r["hil_tagged"].(bool); return }
func (r ReportInfo) IncidentCount() (v *ReportInfoIncident) {
	v, _ = r["incident_count"].(*ReportInfoIncident)
	return
}
func (r *ReportInfo) SetIncidentCount(v *ReportInfoIncident)  { (*r)["incident_count"] = v }
func (r ReportInfo) IsHilRequired() (v bool)                  { v, _ = r["is_hil_required"].(bool); return }
func (r *ReportInfo) SetIsHilRequired(v bool)                 { (*r)["is_hil_required"] = v }
func (r ReportInfo) IsMlRequired() (v bool)                   { v, _ = r["is_ml_required"].(bool); return }
func (r *ReportInfo) SetIsMlRequired(v bool)                  { (*r)["is_ml_required"] = v }
func (r *ReportInfo) SetMlTagged(v bool)                      { (*r)["ml_tagged"] = v }
func (r ReportInfo) MlTagged() (v bool)                       { v, _ = r["ml_tagged"].(bool); return }
func (r *ReportInfo) SetMultiplePeople(v *ReportInfoIncident) { (*r)["multiple_people"] = v }
func (r ReportInfo) MultiplePeople() (v *ReportInfoIncident) {
	v, _ = r["multiple_people"].(*ReportInfoIncident)
	return
}
func (r *ReportInfo) SetStrictCompliance(v bool)                 { (*r)["strict_compliance"] = v }
func (r ReportInfo) StrictCompliance() (v bool)                  { v, _ = r["strict_compliance"].(bool); return }
func (r *ReportInfo) SetSuspiciousObjects(v *ReportInfoIncident) { (*r)["suspicious_objects"] = v }
func (r ReportInfo) SuspiciousObjects() (v *ReportInfoIncident) {
	v, _ = r["suspicious_objects"].(*ReportInfoIncident)
	return
}
func (r *ReportInfo) SetTaggerComment(v string) { (*r)["tagger_comment"] = v }
func (r ReportInfo) TaggerComment() (v string)  { v, _ = r["tagger_comment"].(string); return }
func (r *ReportInfo) SetTaggerReason(v string)  { (*r)["tagger_reason"] = v }
func (r ReportInfo) TaggerReason() (v string)   { v, _ = r["tagger_reason"].(string); return }

type ReportInfoIncident struct {
	Grade string `bson:"grade" json:"grade"`
	Grain string `bson:"grain" json:"grain"`
	Value int    `bson:"sf" json:"sf"`
}
type Review struct {
	SessionId   string            `bson:"session_id" json:"session_id"`
	ReviewerId  string            `bson:"reviewer_id" json:"reviewer_id"`
	IsActive    bool              `bson:"is_active" json:"is_active"`
	Probability BreachProbability `bson:"probability" json:"probability"`
	Reason      string            `bson:"reason" json:"reason"`
	Comment     string            `bson:"comment" json:"comment"`
	Incidents   []*Incident       `bson:"incidents" json:"incidents"`
}
type Session struct {
	SessionId   string                 `bson:"session_id" json:"session_id"`
	ReviewerId  string                 `bson:"tagger_id" json:"reviewer_id"`
	Status      SessionStatus          `bson:"tagging_status" json:"status"`
	Client      string                 `bson:"client" json:"client"`
	Environment string                 `bson:"environment" json:"environment"`
	Tag         string                 `bson:"tag" json:"tag"`
	Metadata    map[string]interface{} `bson:"-" json:"metadata"`
	Dictionary  *BreachDictionary      `bson:"-" json:"dictionary"`
	Review      *Review                `bson:"-" json:"review"`
}
type SessionStatus string
type Source string
type StatusCode string

type AuthenticateReviewerHandler struct {
	Responses struct {
		NoContent struct {
			XAccessToken string "header:\"X-Access-Token\""
		} `status:"204"`
		Unauthorized struct {
			Error *Error `json:"error"`
		} `status:"401"`
		Default struct {
			Error *Error `json:"error"`
		} `status:"0"`
	}
}

func (r *AuthenticateReviewerHandler) ServeHTTP(http.ResponseWriter, *http.Request) {
	panic("not implemented")
}

type CancelReviewHandler struct {
	Responses struct {
		NoContent struct{} `status:"204"`
		NotFound  struct {
			Error *Error `json:"error"`
		} `status:"404"`
		Forbidden struct {
			Error *Error `json:"error"`
		} `status:"403"`
		Default struct {
			Error *Error `json:"error"`
		} `status:"0"`
	}
}

func (r *CancelReviewHandler) ServeHTTP(http.ResponseWriter, *http.Request) { panic("not implemented") }

type ContinueReviewHandler struct {
	Responses struct {
		OK struct {
			Session *Session `json:"session"`
		} `status:"200"`
		NotFound struct {
			Error *Error `json:"error"`
		} `status:"404"`
		Forbidden struct {
			Error *Error `json:"error"`
		} `status:"403"`
		Default struct {
			Error *Error `json:"error"`
		} `status:"0"`
	}
}

func (r *ContinueReviewHandler) ServeHTTP(http.ResponseWriter, *http.Request) {
	panic("not implemented")
}

type CountSessionsHandler struct {
	Responses struct {
		OK struct {
			Count *Count `json:"count"`
		} `status:"200"`
		Forbidden struct {
			Error *Error `json:"error"`
		} `status:"403"`
		Default struct {
			Error *Error `json:"error"`
		} `status:"0"`
	}
}

func (r *CountSessionsHandler) ServeHTTP(http.ResponseWriter, *http.Request) {
	panic("not implemented")
}

type FinishReviewHandler struct {
	Request struct {
		Probability BreachProbability `json:"probability"`
		Reason      string            `json:"reason"`
		Comment     string            `json:"comment"`
		Incidents   []*Incident       `json:"incidents"`
	}
	Responses struct {
		NoContent struct{} `status:"204"`
		NotFound  struct {
			Error *Error `json:"error"`
		} `status:"404"`
		Forbidden struct {
			Error *Error `json:"error"`
		} `status:"403"`
		Default struct {
			Error *Error `json:"error"`
		} `status:"0"`
	}
}

func (r *FinishReviewHandler) ServeHTTP(http.ResponseWriter, *http.Request) { panic("not implemented") }

type RestartReviewHandler struct {
	Request struct {
		Id string `query:"id"`
	}
	Responses struct {
		OK struct {
			Session *Session `json:"session"`
		} `status:"200"`
		NotFound struct {
			Error *Error `json:"error"`
		} `status:"404"`
		Forbidden struct {
			Error *Error `json:"error"`
		} `status:"403"`
		Default struct {
			Error *Error `json:"error"`
		} `status:"0"`
	}
}

func (r *RestartReviewHandler) ServeHTTP(http.ResponseWriter, *http.Request) {
	panic("not implemented")
}

type StartReviewHandler struct {
	Request struct {
		Tag string `query:"tag"`
	}
	Responses struct {
		OK struct {
			Session *Session `json:"session"`
		} `status:"200"`
		SeeOther struct {
			Location string `header:"Location"`
		} `status:"303"`
		NotFound struct {
			Error *Error `json:"error"`
		} `status:"404"`
		Forbidden struct {
			Error *Error `json:"error"`
		} `status:"403"`
		Default struct {
			Error *Error `json:"error"`
		} `status:"0"`
	}
}

func (r *StartReviewHandler) ServeHTTP(http.ResponseWriter, *http.Request) { panic("not implemented") }

type StartReviewSessionHandler struct {
	Request struct {
		Id string `query:"id"`
	}
	Responses struct {
		OK struct {
			Session *Session `json:"session"`
		} `status:"200"`
		SeeOther struct {
			Location string `header:"Location"`
		} `status:"303"`
		NotFound struct {
			Error *Error `json:"error"`
		} `status:"404"`
		Conflict struct {
			Error *Error `json:"error"`
		} `status:"409"`
		Forbidden struct {
			Error *Error `json:"error"`
		} `status:"403"`
		Default struct {
			Error *Error `json:"error"`
		} `status:"0"`
	}
}

func (r *StartReviewSessionHandler) ServeHTTP(http.ResponseWriter, *http.Request) {
	panic("not implemented")
}

type UpsertReviewHandler struct {
	Request struct {
		Incidents []*Incident `json:"incidents"`
	}
	Responses struct {
		NoContent struct{} `status:"204"`
		NotFound  struct {
			Error *Error `json:"error"`
		} `status:"404"`
		Forbidden struct {
			Error *Error `json:"error"`
		} `status:"403"`
		Default struct {
			Error *Error `json:"error"`
		} `status:"0"`
	}
}

func (r *UpsertReviewHandler) ServeHTTP(http.ResponseWriter, *http.Request) { panic("not implemented") }
