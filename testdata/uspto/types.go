package uspto

import (
	"net/http"
)

type DataSetList struct {
	Total int
	Apis  []struct {
		ApiKey              string
		ApiVersionNumber    string
		ApiUrl              string
		ApiDocumentationUrl string
	}
}

type ListDataSetsHandler struct {
	Responses struct {
		OK struct {
			DataSetList *DataSetList `json:"data_set_list"`
		} `status:"200"`
	}
}

func (r *ListDataSetsHandler) ServeHTTP(http.ResponseWriter, *http.Request) { panic("not implemented") }

type ListSearchableFieldsHandler struct {
	Request struct {
		Dataset string `path:"dataset"`
		Version string `path:"version"`
	}
	Responses struct {
		OK       struct{} `status:"200"`
		NotFound struct{} `status:"404"`
	}
}

func (r *ListSearchableFieldsHandler) ServeHTTP(http.ResponseWriter, *http.Request) {
	panic("not implemented")
}

type PerformSearchHandler struct {
	Request struct {
		Version  string `path:"version"`
		Dataset  string `path:"dataset"`
		Criteria string "x-www-form-urlencoded:\"criteria\""
		Start    int    "x-www-form-urlencoded:\"start\""
		Rows     int    "x-www-form-urlencoded:\"rows\""
	}
	Responses struct {
		OK       struct{} `status:"200"`
		NotFound struct{} `status:"404"`
	}
}

func (r *PerformSearchHandler) ServeHTTP(http.ResponseWriter, *http.Request) {
	panic("not implemented")
}
