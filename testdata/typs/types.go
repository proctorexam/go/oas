package typs

import (
	"time"
)

type AllOf struct {
	String string
	Number float64
}
type AnyOf interface{}
type Array []string
type Boolean = bool
type Date = time.Time
type Integer = int
type Nested struct {
	Number     float64
	Array      []string
	ArrayRef   Array
	Map        map[string]interface{}
	SelfRef    *Nested
	SelfRefArr []*Nested
}
type Number = float64
type Object struct{ String string }
type Object2 struct{ String string }
type OneOf interface{}
type OneOfSameType = float64
type OnlyAdditionalProperties map[string]*Object
type Ref = *Object
type String = string
type WithAdditionalProperties map[string]interface{}

func (r WithAdditionalProperties) Get(k string) (v *Object2) { v, _ = r[k].(*Object2); return }
func (r *WithAdditionalProperties) Set(k string, v *Object2) { (*r)[k] = v }
func (r WithAdditionalProperties) Number() (v float64)       { v, _ = r["number"].(float64); return }
func (r *WithAdditionalProperties) SetNumber(v float64)      { (*r)["number"] = v }
