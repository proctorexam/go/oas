package callback_example

import (
	"net/http"
)

type PostStreamsHandler struct {
	Request struct {
		CallbackUrl string `query:"callbackUrl"`
	}
	Responses struct {
		Created struct {
			SubscriptionId string `json:"subscription_id"`
		} `status:"201"`
	}
}

func (r *PostStreamsHandler) ServeHTTP(http.ResponseWriter, *http.Request) { panic("not implemented") }
