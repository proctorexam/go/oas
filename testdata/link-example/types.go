package link_example

import (
	"net/http"
)

const (
	StateDeclined State = "declined"
	StateMerged   State = "merged"
	StateOpen     State = "open"
)

type Pullrequest struct {
	Id         int
	Title      string
	Repository *Repository
	Author     *User
}
type Repository struct {
	Slug  string
	Owner *User
}
type State string
type User struct {
	Username string
	Uuid     string
}

type GetPullRequestsByIdHandler struct {
	Request struct {
		Username string `path:"username"`
		Slug     string `path:"slug"`
		Pid      string `path:"pid"`
	}
	Responses struct {
		OK struct {
			Pullrequest *Pullrequest `json:"pullrequest"`
		} `status:"200"`
	}
}

func (r *GetPullRequestsByIdHandler) ServeHTTP(http.ResponseWriter, *http.Request) {
	panic("not implemented")
}

type GetPullRequestsByRepositoryHandler struct {
	Request struct {
		Username string `path:"username"`
		Slug     string `path:"slug"`
		State    State  `query:"state"`
	}
	Responses struct {
		OK struct{} `status:"200"`
	}
}

func (r *GetPullRequestsByRepositoryHandler) ServeHTTP(http.ResponseWriter, *http.Request) {
	panic("not implemented")
}

type GetRepositoriesByOwnerHandler struct {
	Request struct {
		Username string `path:"username"`
	}
	Responses struct {
		OK struct{} `status:"200"`
	}
}

func (r *GetRepositoriesByOwnerHandler) ServeHTTP(http.ResponseWriter, *http.Request) {
	panic("not implemented")
}

type GetRepositoryHandler struct {
	Request struct {
		Username string `path:"username"`
		Slug     string `path:"slug"`
	}
	Responses struct {
		OK struct {
			Repository *Repository `json:"repository"`
		} `status:"200"`
	}
}

func (r *GetRepositoryHandler) ServeHTTP(http.ResponseWriter, *http.Request) {
	panic("not implemented")
}

type GetUserByNameHandler struct {
	Request struct {
		Username string `path:"username"`
	}
	Responses struct {
		OK struct {
			User *User `json:"user"`
		} `status:"200"`
	}
}

func (r *GetUserByNameHandler) ServeHTTP(http.ResponseWriter, *http.Request) {
	panic("not implemented")
}

type MergePullRequestHandler struct {
	Request struct {
		Username string `path:"username"`
		Slug     string `path:"slug"`
		Pid      string `path:"pid"`
	}
	Responses struct {
		NoContent struct{} `status:"204"`
	}
}

func (r *MergePullRequestHandler) ServeHTTP(http.ResponseWriter, *http.Request) {
	panic("not implemented")
}
