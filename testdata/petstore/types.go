package petstore

import (
	"net/http"
)

type Error struct {
	Code    int32
	Message string
}
type Pet struct {
	Id   int64
	Name string
	Tag  string
}
type Pets []*Pet

type CreatePetsHandler struct {
	Responses struct {
		Created struct{} `status:"201"`
		Default struct {
			Error *Error `json:"error"`
		} `status:"0"`
	}
}

func (r *CreatePetsHandler) ServeHTTP(http.ResponseWriter, *http.Request) { panic("not implemented") }

type ListPetsHandler struct {
	Request struct {
		Limit int32 `query:"limit"`
	}
	Responses struct {
		OK struct {
			XNext string "header:\"x-next\""
			Pets  Pets   `json:"pets"`
		} `status:"200"`
		Default struct {
			Error *Error `json:"error"`
		} `status:"0"`
	}
}

func (r *ListPetsHandler) ServeHTTP(http.ResponseWriter, *http.Request) { panic("not implemented") }

type ShowPetByIdHandler struct {
	Request struct {
		PetId string `path:"petId"`
	}
	Responses struct {
		OK struct {
			Pet *Pet `json:"pet"`
		} `status:"200"`
		Default struct {
			Error *Error `json:"error"`
		} `status:"0"`
	}
}

func (r *ShowPetByIdHandler) ServeHTTP(http.ResponseWriter, *http.Request) { panic("not implemented") }
