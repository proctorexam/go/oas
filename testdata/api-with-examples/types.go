package api_with_examples

import (
	"net/http"
)

type GetVersionDetailsv2Handler struct {
	Responses struct {
		OK                          struct{} `status:"200"`
		NonAuthoritativeInformation struct{} `status:"203"`
	}
}

func (r *GetVersionDetailsv2Handler) ServeHTTP(http.ResponseWriter, *http.Request) {
	panic("not implemented")
}

type ListVersionsv2Handler struct {
	Responses struct {
		OK              struct{} `status:"200"`
		MultipleChoices struct{} `status:"300"`
	}
}

func (r *ListVersionsv2Handler) ServeHTTP(http.ResponseWriter, *http.Request) {
	panic("not implemented")
}
