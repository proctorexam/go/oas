package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/proctorexam/go/oas/types"
)

// usage: go run gitlab.com/proctorexam/go/oas FILENAME
var (
	typesEnabled    = flag.Bool("types", true, "types")
	handlersEnabled = flag.Bool("handlers", false, "handlers")
	pkgName         = flag.String("pkg", "", "pkg")
)

func main() {
	flag.Parse()
	filename := flag.Arg(0)
	if filename == "" {
		fmt.Println("usage: go run gitlab.com/proctorexam/go/oas FILENAME")
		return
	}
	dir := filepath.Dir(filename)
	name := strings.TrimSuffix(filepath.Base(filename), filepath.Ext(filename))
	types.DefaultImporter.Types = *typesEnabled
	types.DefaultImporter.OperationHandlers = *handlersEnabled
	pkg, err := types.DefaultImporter.ImportFrom(name, dir, 0)
	if err != nil {
		panic(err)
	}
	if *pkgName != "" {
		pkg.SetName(*pkgName)
	}
	os.Stdout.WriteString(types.PackageString(pkg, types.DefaultImporter.Impl(pkg)))
}
