# OAS

## openapi schema to go type conversion

keywords under `go/type` columns

`type = basic | pointer | array | slice | map | struct | named | any` as defined in [go/types](https://github.com/golang/example/tree/master/gotypes#types)

`interface{...}` is a `named` type with a `method set` consist of getter/setter functions for the schema type.

### primitive / basic type conversion

| `schema/type` | `schema/format` | `go/type`   |
| ------------- | --------------- | ----------- |
| `integer`     |                 | `int`       |
| `integer`     | `int32`         | `int32`     |
| `integer`     | `int64`         | `int64`     |
| `number`      | `float`         | `float32`   |
| `number`      | `double`        | `float64`   |
| `string`      |                 | `string`    |
| `string`      | `byte`          | `byte`      |
| `string`      | `binary`        | `[]byte`    |
| `string`      | `date`          | `time.Time` |
| `string`      | `date-time`     | `time.Time` |
| `boolean`     |                 | `bool`      |

### object type conversion

| `schema/type` | `schema/additionalProperties` | `schema/properties` | `go/type`                   |
| ------------- | ----------------------------- | ------------------- | --------------------------- |
| `object`      |                               |                     | `map[string]any`            |
| `object`      | `false`                       |                     |                             |
| `object`      | `true`                        |                     | `map[string]any`            |
| `object`      | `schema OR ref`               |                     | `map[string]type`           |
| `object`      |                               | `schema OR ref`     | `type`                      |
| `object`      | `false`                       | `schema OR ref`     | `type`                      |
| `object`      | `true`                        | `schema OR ref`     | `map[string]interface{...}` |
| `object`      | `schema OR ref`               | `schema OR ref`     | `map[string]interface{...}` |

### array type conversion

| `schema/type` | `schema/items`  | `go/type` |
| ------------- | --------------- | --------- |
| `array`       |                 |           |
| `array`       | `array`         |           |
| `array`       | `schema OR ref` | `[]type`  |

### allOf conversion

allOf items MUST be same type, items length MUST be > 1

| `schema/[]allOf` | `go/type` |
| ---------------- | --------- |
| `type`           | `type`    |
| `[]type`         |           |

### oneOf conversion

oneOf items CANNOT be same type, items length MUST be > 1

| `schema/[]oneOf` | `go/type` |
| ---------------- | --------- |
| `type`           |           |
| `[]type`         | `any`     |

### anyOf conversion

anyOf items CANNOT be same type, items length MUST be > 1

| `schema/[]anyOf` | `go/type` |
| ---------------- | --------- |
| `type`           | `type`    |
| `[]type`         | `any`     |

### ref conversion

references to `struct` types becomes `pointer` types, eg; `type Ref = *Pointer`
all other all references becomes direct `alias` types, eg; `type Ref = string`
