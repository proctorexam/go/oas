package types

import (
	"bytes"
	"fmt"
	"go/format"
	"go/types"
	"regexp"
	"sort"
	"strings"

	"gitlab.com/proctorexam/go/oas/internal/stringcases"
	"golang.org/x/tools/go/types/typeutil"
)

type constMap struct {
	consts    map[string][]*types.Const
	constKeys []string
}

func newConstMap() *constMap {
	consts := make(map[string][]*types.Const)
	constKeys := make([]string, 0)
	return &constMap{consts: consts, constKeys: constKeys}
}

var tagr1 = regexp.MustCompile(`"(\w+):\\"(\w+|-)\\""`)
var tagrv1 = "`$1:\"$2\"`"
var tagr2 = regexp.MustCompile(` (\w+):\\"(\w+|-)\\""`)
var tagrv2 = " $1:\"$2\"`"
var tagr3 = regexp.MustCompile(`"(\w+):\\"(\w+|-)\\" `)
var tagrv3 = "`$1:\"$2\" "

// PackageString takes p package and implementation map impl and returns go source code of them
func PackageString(p *types.Package, impl map[string]string) string {
	buf := new(bytes.Buffer)
	q := types.RelativeTo(p)

	fmt.Fprintln(buf, "package ", stringcases.Snake(stringcases.Export(p.Name())))
	fmt.Fprintln(buf, "")

	if imps := p.Imports(); len(imps) > 0 {
		fmt.Fprintln(buf, "import(")
		for _, i := range imps {
			fmt.Fprintln(buf, `"`+i.Path()+`"`)
		}
		fmt.Fprintln(buf, ")")
	}
	fmt.Fprintln(buf, "")

	sbuf := new(bytes.Buffer)
	cm := writeScope(sbuf, p, p.Scope(), impl, q)
	writeConstMap(buf, cm, q)
	buf.Write(sbuf.Bytes())
	buf.WriteString("\n")

	for i := 0; i < p.Scope().NumChildren(); i++ {
		sbuf.Reset()
		cm = writeScope(sbuf, p, p.Scope().Child(i), impl, q)
		writeConstMap(buf, cm, q)
		buf.Write(sbuf.Bytes())
	}

	b, err := format.Source(buf.Bytes())
	if err != nil {
		fmt.Println(buf.String())
		panic(err)
	}
	str := string(b)

	str = tagr1.ReplaceAllString(str, tagrv1)
	str = tagr2.ReplaceAllString(str, tagrv2)
	str = tagr3.ReplaceAllString(str, tagrv3)
	return str
}

func writeConstMap(buf *bytes.Buffer, cm *constMap, q types.Qualifier) {
	// render consts in some way
	if l := len(cm.consts); l > 0 {
		// sort keys so output will be consistent
		sort.Strings(cm.constKeys)

		fmt.Fprintln(buf, "const(")
		for _, k := range cm.constKeys {
			c := cm.consts[k]
			for _, cv := range c {
				tStr := types.TypeString(cv.Type(), q)
				v := cv.Val()
				fmt.Fprintln(buf, cv.Name(), tStr, " = ", v)
			}
			fmt.Fprintln(buf, "")
		}
		fmt.Fprintln(buf, ")")
	}
}

func writeScope(buf *bytes.Buffer, p *types.Package, scope *types.Scope, impl map[string]string, q types.Qualifier) *constMap {
	cm := newConstMap()
	c := &typeutil.MethodSetCache{}

	for _, n := range scope.Names() {
		obj := scope.Lookup(n)

		if obj.Pkg() != p {
			continue
		}

		if ct, ok := obj.(*types.Const); ok {
			ck := ct.Type().String()

			if _, ok := cm.consts[ck]; !ok {
				cm.consts[ck] = make([]*types.Const, 0)
				cm.constKeys = append(cm.constKeys, ck)
			}
			cm.consts[ck] = append(cm.consts[ck], ct)
			continue
		}

		tn, ok := obj.(*types.TypeName)
		if !ok || tn.Pkg() != p {
			continue
		}

		typestr := types.ObjectString(obj, q)
		isAlias := strings.Contains(typestr, "=")
		// fix for types.ObjectString won't output built in aliases discrepency
		if tn.IsAlias() && !isAlias {
			fmt.Fprintln(buf, typestr, "=", tn.Type().Underlying())
			continue
		}
		// print type as usual
		fmt.Fprintln(buf, typestr)
		// since it can be still alias skip methods if so, can also be checked with tn.IsAlias()
		if isAlias {
			continue
		}

		mset := typeutil.IntuitiveMethodSet(obj.Type(), c)
		// note, Get and Set methods names defined in oas and this code should keep track of the change of them if ever happens
		var prefixRe = regexp.MustCompile("Get|Set")
		sort.Slice(mset, func(i, j int) bool {
			a := prefixRe.ReplaceAllString(mset[i].Obj().Name(), "")
			b := prefixRe.ReplaceAllString(mset[j].Obj().Name(), "")
			return a < b
		})
		for _, sel := range mset {
			f, ok := sel.Obj().(*types.Func)
			if !ok {
				panic(fmt.Errorf("want *types.Func got %s", sel))
			}
			// implement method sets if any
			funcBody, ok := impl[f.FullName()]
			if !ok {
				funcBody = `panic("not implemented")`
			}
			// get method string representation
			methstr := types.SelectionString(sel, (*types.Package).Name)
			// default string is type specific, which needs small tweak to usual go code
			methstr = strings.Replace(methstr, "method (", "func (r ", -1)
			// since qualifier uses not relative to this package but package name,
			// this package name better to be removed
			methstr = strings.ReplaceAll(methstr, p.Name()+".", "")
			// finally add function body
			fmt.Fprintln(buf, methstr+"{"+funcBody+"}")
		}
	}
	return cm
}
