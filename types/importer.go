package types

import (
	"fmt"
	"go/constant"
	"go/importer"
	"go/token"
	"go/types"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/valyala/fastjson"
	"gitlab.com/proctorexam/go/oas/internal/jsonschema"
	"gitlab.com/proctorexam/go/oas/internal/stringcases"
)

// Default exports default importerFrom instance
var DefaultImporter = NewImporterFrom()

// string case function aplies all json fields, struct tags, map keys, ...
var JsonPropCase = stringcases.Snake

// importerFrom implements ImporterFrom interface for importing types from open api (3.0.x) specs
type importerFrom struct {
	Types             bool
	OperationHandlers bool
	pkgs              map[string]*types.Package
	docs              map[string]*fastjson.Value
	impl              map[*types.Package]map[string]string
}

// NewImporter returns new importerFrom instance to use with OAS specification files
func NewImporterFrom() *importerFrom {
	return &importerFrom{
		Types: true,
		pkgs:  make(map[string]*types.Package),
		docs:  make(map[string]*fastjson.Value),
		impl:  make(map[*types.Package]map[string]string),
	}
}

func (i *importerFrom) Import(path string) (*types.Package, error) {
	return i.ImportFrom(path, filepath.Dir(path), 0)
}
func (i *importerFrom) ImportFrom(path, dir string, mode types.ImportMode) (*types.Package, error) {
	pkg, ok := i.pkgs[path]
	if ok {
		return pkg, nil
	}
	doc, ok := i.docs[path]
	if !ok {
		d, err := jsonschema.ParseFile(filepath.Join(dir, path+".json"))
		if err != nil {
			return nil, err
		}
		i.docs[path] = d
		doc = d
	}
	pkg = types.NewPackage(path, filepath.Base(path))
	i.pkgs[path] = pkg
	i.impl[pkg] = make(map[string]string)

	pkgImporter := &oasImporter{p: pkg, d: doc, impl: i.impl[pkg]}
	if i.Types {
		pkgImporter.importSchemaTypes()
	}
	if i.OperationHandlers {
		pkgImporter.importOperationHandlers()
	}

	pkg.MarkComplete()
	return pkg, nil
}

// Impl returns map of type name and their implementation code
// For instance, generated types for object with additional properties
// have method set to support arbitrary type safe key/value access
// and their func bodies stored in impl maps
func (i *importerFrom) Impl(p *types.Package) map[string]string {
	return i.impl[p]
}

// Debug prints summary of the package
func (i *importerFrom) Debug() {
	for _, p := range i.pkgs {
		fmt.Printf("Package  %q\n", p.Path())
		fmt.Printf("Name:    %s\n", p.Name())
		fmt.Printf("Imports: %s\n", p.Imports())
		fmt.Printf("Scope:   %s\n", p.Scope())
	}
}

// oasImporter provides methods for creating go/types package types from open api json documents
type oasImporter struct {
	p    *types.Package
	d    *fastjson.Value
	refs map[string]types.Type
	impl map[string]string
}

// importSchemaTypes imports schema types from open api document into go package scope
func (i *oasImporter) importOperationHandlers() {
	scope := types.NewScope(i.p.Scope(), nop, nop, "")
	var parameters []*fastjson.Value
	i.d.GetObject("paths").Visit(func(pathK []byte, pathItem *fastjson.Value) {
		// parameters = pathItem.GetArray("parameters")

		pathItem.GetObject().Visit(func(methodK []byte, operation *fastjson.Value) {
			fields := make([]*types.Var, 0)

			// FIX ME: pathItem parameters can get duplicated in operation and it's ok in the open api schema
			// thus we do not use path parameters defined globally but ones defined only in operation itself
			parameters = operation.GetArray("parameters") // append(parameters, operation.GetArray("parameters")...)
			hasBody := operation.Exists("requestBody")
			hasParameters := len(parameters) > 0
			if hasParameters || hasBody {
				rFields := make([]*types.Var, 0)
				rTags := make([]string, 0)

				if hasParameters {
					for _, p := range parameters {
						p = jsonschema.Resolve(i.d, p)
						pIn := string(p.GetStringBytes("in"))
						pName := string(p.GetStringBytes("name"))
						pSchema := p.Get("schema")
						if !pSchema.Exists("x-name") {
							pSchema.Set("x-name", fastjson.MustParse(`"`+pName+`"`))
						}
						pType := i.newType(pSchema)
						pField := types.NewField(nop, i.p, eName([]byte(pName)), pType, false)
						pTag := fmt.Sprintf(`%s:"%s"`, pIn, pName)
						rFields = append(rFields, pField)
						rTags = append(rTags, pTag)
					}
				}

				if hasBody {
					requestBody := jsonschema.Resolve(i.d, operation.Get("requestBody"))
					var contentType string
					var bSchema *fastjson.Value
					requestBody.GetObject("content").Visit(func(contentTypeK []byte, content *fastjson.Value) {
						if bSchema != nil {
							panic(fmt.Errorf("multiple content types in requestBody not supported, found in: %s %s", pathK, methodK))
						}
						c := strings.Split(string(contentTypeK), "/")
						contentType = c[len(c)-1]
						bSchema = content.Get("schema")
					})
					if bSchema == nil {
						panic(fmt.Errorf("supported content type in requestBody not found, found in: %s %s", pathK, methodK))
					}
					bType := i.newType(bSchema)
					bPointer, ok := bType.(*types.Pointer)
					if ok {
						bpName := bPointer.Elem().(*types.Named).Obj().Name()
						bpField := types.NewField(nop, i.p, bpName, bPointer, false)
						rFields = append(rFields, bpField)
						rTags = append(rTags, fmt.Sprintf(`%s:"%s"`, JsonPropCase(contentType), JsonPropCase(bpName)))
					} else {
						bNamed, ok := bType.(*types.Named)
						if ok {
							bpName := bNamed.Obj().Name()
							bpField := types.NewField(nop, i.p, bpName, bNamed, false)
							rFields = append(rFields, bpField)
							rTags = append(rTags, fmt.Sprintf(`%s:"%s"`, JsonPropCase(contentType), JsonPropCase(bpName)))
						} else {
							bStruct, ok := bType.(*types.Struct)
							if !ok {
								panic(fmt.Errorf("request body must be an object or reference to an object: got %s", bSchema))
							}
							for n := 0; n < bStruct.NumFields(); n++ {
								bField := bStruct.Field(n)
								rFields = append(rFields, bField)
								rTag := bStruct.Tag(n)
								if rTag == "" {
									rTag = fmt.Sprintf(`%s:"%s"`, JsonPropCase(contentType), JsonPropCase(bField.Name()))
								}
								rTags = append(rTags, rTag)
							}
						}
					}
				}
				fields = append(fields, types.NewField(nop, i.p, "Request", types.NewStruct(rFields, rTags), false))

			}

			if operation.Exists("responses") {
				resFields := make([]*types.Var, 0)
				resTags := make([]string, 0)
				operation.GetObject("responses").Visit(func(codeK []byte, res *fastjson.Value) {
					rFields := make([]*types.Var, 0)
					rTags := make([]string, 0)
					res = jsonschema.Resolve(i.d, res)
					if res.Exists("headers") {
						res.GetObject("headers").Visit(func(nameK []byte, p *fastjson.Value) {
							p = jsonschema.Resolve(i.d, p)
							pIn := "header"
							pName := string(nameK)
							pSchema := p.Get("schema")
							if !pSchema.Exists("x-name") {
								pSchema.Set("x-name", fastjson.MustParse(`"`+pName+`"`))
							}
							pType := i.newType(pSchema)
							pField := types.NewField(nop, i.p, eName([]byte(pName)), pType, false)
							pTag := fmt.Sprintf(`%s:"%s"`, pIn, pName)
							rFields = append(rFields, pField)
							rTags = append(rTags, pTag)
						})
					}

					if res.Exists("content") {
						var contentType string
						var bSchema *fastjson.Value
						res.GetObject("content").Visit(func(contentTypeK []byte, content *fastjson.Value) {
							if bSchema != nil {
								panic(fmt.Errorf("multiple content types in responseBody not supported, found in: %s %s", pathK, methodK))
							}
							c := strings.Split(string(contentTypeK), "/")
							contentType = c[len(c)-1]
							bSchema = content.Get("schema")
						})
						if bSchema == nil {
							// edge case, response body has content type, but no schema, probably has examples
							// although this is invalid, it's supported by openapi, so assume any type in this case
							bSchema = fastjson.MustParse(`{"type":"object","additionalProperties":true}`)
							// panic(fmt.Errorf("supported content type in responseBody not found, found in: %s %s", pathK, methodK))
						}
						bType := i.newType(bSchema)
						bPointer, ok := bType.(*types.Pointer)
						if ok {
							bpName := bPointer.Elem().(*types.Named).Obj().Name()
							bpField := types.NewField(nop, i.p, bpName, bPointer, false)
							rFields = append(rFields, bpField)
							rTags = append(rTags, fmt.Sprintf(`%s:"%s"`, JsonPropCase(contentType), JsonPropCase(bpName)))
						} else {
							bNamed, ok := bType.(*types.Named)
							if ok {
								bpName := bNamed.Obj().Name()
								bpField := types.NewField(nop, i.p, bpName, bNamed, false)
								rFields = append(rFields, bpField)
								rTags = append(rTags, fmt.Sprintf(`%s:"%s"`, JsonPropCase(contentType), JsonPropCase(bpName)))
							} else {
								bStruct, ok := bType.(*types.Struct)
								if ok {
									for n := 0; n < bStruct.NumFields(); n++ {
										bField := bStruct.Field(n)
										rFields = append(rFields, bField)
										rTag := bStruct.Tag(n)
										if rTag == "" {
											rTag = fmt.Sprintf(`%s:"%s"`, JsonPropCase(contentType), JsonPropCase(bField.Name()))
										}
										rTags = append(rTags, rTag)
									}
								}

							}
						}
					}
					rName := "Default"
					scode, err := strconv.Atoi(string(codeK))
					if err == nil {
						rName = stringcases.Export(http.StatusText(scode))
					}
					if rName == "" {
						panic(fmt.Errorf("invalid response key, found in: %s %s", pathK, methodK))
					}

					resFields = append(resFields, types.NewField(nop, i.p, rName, types.NewStruct(rFields, rTags), false))
					resTags = append(resTags, fmt.Sprintf(`status:"%v"`, scode))
				})
				fields = append(fields, types.NewField(nop, i.p, "Responses", types.NewStruct(resFields, resTags), false))
			}

			opStruct := types.NewStruct(fields, nil)

			opName := eName(operation.GetStringBytes("operationId"))
			if opName == "" {
				opName = fmt.Sprintf("%s%s", eName(methodK), eName(pathK))
				// panic(fmt.Errorf("operationId must be provided = path: %s, method: %s", pathK, methodK))
			}
			opType := types.NewNamed(types.NewTypeName(nop, i.p, opName+"Handler", nil), opStruct, nil)
			i.implement("net/http", "Handler", opType)
			scope.Insert(opType.Obj())
		})
	})
}

// importSchemaTypes imports schema types from open api document into go package scope
func (i *oasImporter) importSchemaTypes() {
	i.d.GetObject("components", "schemas").Visit(func(k []byte, s *fastjson.Value) {
		t := i.newNamedType(k, s).(*types.Named)
		i.registerNamedType(t)
	})
}

// newNamedType returns go/types.Named for given name and schema
func (i *oasImporter) newNamedType(k []byte, s *fastjson.Value) types.Type {
	var nType types.Type
	uType := i.newType(s)

	if isAlias(i.p, uType, s) {
		nType = uType
	}
	if _, ok := uType.(*types.Named); ok {
		uType = uType.Underlying()
	}

	tName := types.NewTypeName(nop, i.p, eName(k), nType)
	return types.NewNamed(tName, uType, nil)
}

// registerNamedType inserts named type into pakcage scope
func (i *oasImporter) registerNamedType(nt *types.Named) {
	i.insert(nt.Obj())
}

// insert inserts object into pakcage scope
func (i *oasImporter) insert(o types.Object) {
	i.p.Scope().Insert(o)
}

// newType returns go type for given schema
// returning type can be any type satisfied types.Type interface
func (i *oasImporter) newType(s *fastjson.Value) types.Type {
	var t types.Type
	if xType := string(s.GetStringBytes("x-type")); xType != "" {
		t = types.NewNamed(types.NewTypeName(nop, i.p, xType, nil), tAny(), nil)
		if im := string(s.GetStringBytes("x-import")); im != "" {
			addImport(i.p, types.NewPackage(im, strings.Split(xType, ".")[0]))
		}
		return t
	}

	switch string(s.GetStringBytes("type")) {
	case "boolean":
		t = types.Typ[types.Bool]
		if !s.Exists("enum") {
			return t
		}
		return i.constType(s, t, func(i int, c string, e *fastjson.Value) any {
			return e.GetBool()
		})
	case "integer":
		switch string(s.GetStringBytes("format")) {
		case "int32":
			t = types.Typ[types.Int32]
		case "int64":
			t = types.Typ[types.Int64]
		default:
			t = types.Typ[types.Int]
		}
		if !s.Exists("enum") {
			return t
		}
		return i.constType(s, t, func(i int, c string, e *fastjson.Value) any {
			if e.Type() == fastjson.TypeNumber {
				return e.GetInt64()
			}
			return int64(i)
		})
	case "number":
		switch string(s.GetStringBytes("format")) {
		case "float":
			t = types.Typ[types.Float32]
		case "double":
			t = types.Typ[types.Float64]
		default:
			t = types.Typ[types.Float64]
		}
		if !s.Exists("enum") {
			return t
		}
		return i.constType(s, t, func(i int, c string, e *fastjson.Value) any {
			return e.GetFloat64()
		})
	case "string":
		switch string(s.GetStringBytes("format")) {
		case "date", "date-time":
			addImport(i.p, mustImport("time"))
			t = mustImport("time").Scope().Lookup("Time").Type()
		case "binary":
			t = types.NewSlice(types.Typ[types.Byte])
		case "byte":
			t = types.Typ[types.Byte]
		default:
			t = types.Typ[types.String]
		}
		if !s.Exists("enum") {
			return t
		}
		return i.constType(s, t, func(i int, c string, e *fastjson.Value) any {
			return string(e.GetStringBytes())
		})
	case "array":
		items := s.Get("items")
		if items == nil {
			panic("array items cannot be nil")
		}
		return types.NewSlice(i.newType(items))
	case "object":
		ap := s.Get("additionalProperties")
		ps := s.Get("properties")
		switch {
		case s.Exists("x-any"):
			return tAny()
		case ap.GetBool() && ps == nil:
			return types.NewMap(types.Typ[types.String], tAny())
		case ap.GetBool() && ps != nil:
			return i.newInterfaceMapType(s, ps)
		case ap != nil && ps == nil:
			return types.NewMap(types.Typ[types.String], i.newType(ap))
		case ap != nil && ps != nil:
			return i.newInterfaceMapType(s, ps, ap)
		case ap == nil && ps != nil:
			return i.newStructType(s)
		default:
			return types.NewMap(types.Typ[types.String], tAny())
			// panic(fmt.Errorf("object type must have either properties or additionalProperties object: %s", s))
		}
	default:
		switch {
		case s.Exists("$ref"):
			return i.newFromReferencedType(s)
		case s.Exists("allOf"):
			return i.newCompositeType(s, s.GetArray("allOf"))
		case s.Exists("oneOf"):
			return i.newUnionType(s, s.GetArray("oneOf"))
		case s.Exists("anyOf"):
			return i.newUnionType(s, s.GetArray("anyOf"))
		default:
			// edge case, schema has no type, but have "properties" defined, like in callbacks
			// infer object and retry
			if s.Exists("properties") {
				s.Set("type", fastjson.MustParse(`"object"`))
				return i.newType(s)
			}
			panic(fmt.Errorf("cannot process invalid schema type: %s", s))
		}
	}
}

// constType used for generating constants for enums
func (i *oasImporter) constType(s *fastjson.Value, t types.Type, val func(i int, c string, e *fastjson.Value) any) types.Type {
	if !s.Exists("enum") {
		return t
	}
	n := s.GetStringBytes("x-name")
	tt := types.NewNamed(types.NewTypeName(token.NoPos, i.p, eName(n), nil), t, nil)
	enames := s.GetArray("x-enum-names")
	for x, enum := range s.GetArray("enum") {
		var c string
		if x < len(enames) {
			c = stringcases.Export(string(enames[x].GetStringBytes()))
		} else {
			c = eName(n) + stringcases.Export(string(enum.GetStringBytes()))
		}
		if c == "" {
			panic(fmt.Errorf("enum name missing: %s", s))
		}
		v := val(x, c, enum)
		i.insert(
			types.NewConst(token.NoPos, i.p, c, tt, constant.Make(v)),
		)
	}
	i.registerNamedType(tt)
	return tt
}

// newFromReferencedType returns go type for the schema referenced in $ref property
// ref link must be local to the file, multi file or network links not supported atm
func (i *oasImporter) newFromReferencedType(s *fastjson.Value) types.Type {
	refstr := string(s.GetStringBytes("$ref"))
	if strings.Contains(refstr, "properties") {
		r := jsonschema.RefValue(i.d, refstr)
		return i.newType(r)
	}
	if cached, ok := i.refs[refstr]; ok {
		return cached
	}
	if i.refs == nil {
		i.refs = make(map[string]types.Type)
	}
	var t types.Type
	i.refs[refstr] = t
	r := jsonschema.RefValue(i.d, refstr)
	n := r.GetStringBytes("x-name")
	if string(n) == "" {
		panic(fmt.Errorf("x-name extension required on schema values: %s", s))
	}
	uType := i.newNamedType(n, r)
	if isAddressable(uType) {
		i.refs[refstr] = types.NewPointer(uType)
	} else {
		i.refs[refstr] = uType
	}

	return i.refs[refstr]
}

// newStructType returns struct type for the schema with fields resolved recursively
func (i *oasImporter) newStructType(s *fastjson.Value) types.Type {
	props := s.GetObject("properties")
	fields := make([]*types.Var, 0, props.Len())
	tags := make([]string, 0, props.Len())
	props.Visit(func(k []byte, fs *fastjson.Value) {
		uType := i.newType(fs)
		if isAddressable(uType) {
			if !isImported(i.p, uType.(*types.Named)) {
				i.registerNamedType(uType.(*types.Named))
				uType = types.NewPointer(uType)
			}
		}
		embedded := false
		fields = append(fields, types.NewField(nop, i.p, eName(k), uType, embedded))
		tag := fs.Get("x-struct-tag")
		switch {
		case tag != nil && tag.Type() == fastjson.TypeString:
			tags = append(tags, string(tag.GetStringBytes()))
		case fs.Exists("x-struct-tag-json"):
			tags = append(tags, fmt.Sprintf(`json:"%s"`, JsonPropCase(string(k))))
		default:
			tags = append(tags, "")
		}
	})
	return types.NewStruct(fields, tags)
}

// newInterfaceMapType creates a named type with map[string]any underlying type and a method set
// attached for the properties/additionalProperties consist of getter setter methods
func (i *oasImporter) newInterfaceMapType(s, ps *fastjson.Value, ap ...*fastjson.Value) types.Type {
	mType := i.newRecvType(s, types.NewMap(types.Typ[types.String], tAny()))
	add := func(pname string, fs *fastjson.Value) {
		t := i.newType(fs)
		i.addGetterSetter(mType, t, pname)
	}
	ps.GetObject().Visit(func(k []byte, fs *fastjson.Value) {
		add(eName(k), fs)
	})
	for _, p := range ap {
		add("", p)
	}
	i.registerNamedType(mType)
	return mType
}

// newCompositeType merges sub schemas defined in allOf array and returns single type for the merged final schema
// allOf items must have same type or at least one single type
func (i *oasImporter) newCompositeType(s *fastjson.Value, subs []*fastjson.Value) types.Type {
	if jsonschema.ContainsMultipleTypes(i.d, subs) {
		panic("ilogical composition: subs schemas have multiple different types")
	}
	return i.newType(jsonschema.MergeSchemas(i.d, subs))
}

// newUnionType conditionally returns composite or named empty interface type for oneOf and/or anyOf definitions
func (i *oasImporter) newUnionType(s *fastjson.Value, subs []*fastjson.Value) types.Type {
	if !jsonschema.ContainsMultipleTypes(i.d, subs) {
		return i.newCompositeType(s, subs)
	}

	mType := i.newRecvType(s, tAny())
	i.registerNamedType(mType)
	return mType
}

// newRecvType returns named type used as a receiver of newInterfaceMapType
func (i *oasImporter) newRecvType(s *fastjson.Value, uType types.Type) (mType *types.Named) {
	name := eName(s.GetStringBytes("x-name"))
	if name == "" {
		panic(fmt.Errorf("x-name extension required to call newRecvType on schema values"))
	}
	mType = types.NewNamed(types.NewTypeName(nop, i.p, name, nil), uType, nil)
	return
}

const (
	pKey  = "k"
	pVal  = "v"
	pRecv = "r"
	mGet  = "Get"
	mSet  = "Set"
)

// addGetterSetter helper method creates type safe key/value accessor methods for newInterfaceMapType
// it also writes implementations of func bodies into impl map
func (i *oasImporter) addGetterSetter(mType *types.Named, t types.Type, pname string) {
	keyParam := types.NewParam(nop, i.p, pKey, types.Typ[types.String])
	valParam := types.NewParam(nop, i.p, pVal, t)
	pName := `"` + JsonPropCase(pname) + `"`

	// getter
	mRecv := types.NewVar(nop, i.p, pRecv, mType)
	gSig := types.NewSignatureType(mRecv, nil, nil, nil, types.NewTuple(valParam), false)
	gName := pname
	if pname == "" {
		gSig = types.NewSignatureType(mRecv, nil, nil, types.NewTuple(keyParam), types.NewTuple(valParam), false)
		gName = mGet
		pName = pKey
	}
	gFunc := types.NewFunc(nop, i.p, gName, gSig)
	// func body
	i.impl[gFunc.FullName()] = fmt.Sprintf(`%s, _ = %s[%s].(%s); return`, pVal, pRecv, pName, types.TypeString(t, types.RelativeTo(i.p)))
	mType.AddMethod(gFunc)

	// setter
	sRecv := types.NewVar(nop, mRecv.Pkg(), mRecv.Name(), types.NewPointer(mRecv.Type()))
	sSig := types.NewSignatureType(sRecv, nil, nil, types.NewTuple(valParam), nil, false)
	sName := mSet + pname
	if pname == "" {
		sSig = types.NewSignatureType(sRecv, nil, nil, types.NewTuple(keyParam, valParam), nil, false)
		sName = mSet
	}
	sFunc := types.NewFunc(nop, i.p, sName, sSig)
	// func body
	i.impl[sFunc.FullName()] = fmt.Sprintf(`(*%s)[%s]=%s`, pRecv, pName, pVal)
	mType.AddMethod(sFunc)
}

func (i *oasImporter) implement(pkgPath string, iName string, t *types.Named) []*types.Func {
	pkg := mustImport(pkgPath)
	addImport(i.p, pkg)

	iface := pkg.Scope().Lookup(iName).Type().Underlying().(*types.Interface)
	mset := types.NewMethodSet(iface)
	ms := make([]*types.Func, mset.Len())
	for n := 0; n < mset.Len(); n++ {
		m := mset.At(n).Obj().(*types.Func)
		s := m.Type().(*types.Signature)
		r := types.NewVar(nop, i.p, "r", types.NewPointer(t))
		rs := types.NewSignatureType(r, nil, nil, s.Params(), s.Results(), s.Variadic())
		ms[n] = types.NewFunc(nop, i.p, m.Name(), rs)
		t.AddMethod(ms[n])
	}
	return ms
}

// isAlias reports wheather type being converted as alias
func isAlias(p *types.Package, t types.Type, s *fastjson.Value) bool {
	if s.Exists("enum") {
		return false
	}
	switch tt := t.(type) {
	case *types.Struct, *types.Slice, *types.Map, *types.Interface:
		return false
	case *types.Named:
		if _, ok := t.Underlying().(*types.Basic); ok {
			return true
		}
		if isImported(p, tt) {
			return true
		}
		return false
	}

	return true
}

// isStruct reports if given t or it's underlying type is struct
func isStruct(t types.Type) (ok bool) {
	_, ok = t.(*types.Struct)
	if !ok {
		_, ok = t.Underlying().(*types.Struct)
	}
	return
}

// isAddressable reports if uType should be wrapped with a pointer type
func isAddressable(uType types.Type) bool {
	if _, ok := uType.(*types.Named); ok && isStruct(uType.Underlying()) {
		return true
	}
	return false
}

// isImported returns true if t is imported from another package
func isImported(p *types.Package, t *types.Named) bool {
	return t.Obj().Pkg() != p
}

// addImport imports pkg i into pkg p if not imported before
func addImport(p, i *types.Package) {
	imp := p.Imports()
	for _, ip := range imp {
		if i == ip {
			return
		}
	}
	p.SetImports(append(imp, i))
}

// eName converts k bytes into ExporterName string representation
func eName(k []byte) string {
	return stringcases.Export(string(k))
}

const nop = token.NoPos

// defCfg used for importing stdlib packages
var defCfg = &types.Config{Importer: importer.Default()}

func tAny() types.Type { return types.NewInterfaceType(nil, nil) }

func mustImport(pkgPath string) *types.Package {
	p, err := defCfg.Importer.Import(pkgPath)
	if err != nil {
		panic(err)
	}
	return p
}
