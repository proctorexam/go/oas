package types

import (
	"go/types"
	"os"
	"testing"
)

func TestImportExport(t *testing.T) {
	type args struct {
		path string
		dir  string
		mode types.ImportMode
	}
	tests := []struct {
		name     string
		importer types.ImporterFrom
		args     args
		want     string
		wantErr  bool
	}{
		{
			"typs",
			DefaultImporter,
			args{path: "typs", dir: "../testdata", mode: 0},
			string(mustReadFile("../testdata/typs/types.go")),
			false,
		},
		{
			"review",
			DefaultImporter,
			args{path: "review", dir: "../testdata", mode: 0},
			string(mustReadFile("../testdata/review/types.go")),
			false,
		},
		{
			"api-with-examples",
			DefaultImporter,
			args{path: "api-with-examples", dir: "../testdata", mode: 0},
			string(mustReadFile("../testdata/api-with-examples/types.go")),
			false,
		},
		{
			"callback-example",
			DefaultImporter,
			args{path: "callback-example", dir: "../testdata", mode: 0},
			string(mustReadFile("../testdata/callback-example/types.go")),
			false,
		},
		{
			"link-example",
			DefaultImporter,
			args{path: "link-example", dir: "../testdata", mode: 0},
			string(mustReadFile("../testdata/link-example/types.go")),
			false,
		},
		{
			"petstore-expanded",
			DefaultImporter,
			args{path: "petstore-expanded", dir: "../testdata", mode: 0},
			string(mustReadFile("../testdata/petstore-expanded/types.go")),
			false,
		},
		{
			"petstore",
			DefaultImporter,
			args{path: "petstore", dir: "../testdata", mode: 0},
			string(mustReadFile("../testdata/petstore/types.go")),
			false,
		},
		{
			"uspto",
			DefaultImporter,
			args{path: "uspto", dir: "../testdata", mode: 0},
			string(mustReadFile("../testdata/uspto/types.go")),
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := tt.importer.(*importerFrom)
			i.Types = true
			i.OperationHandlers = true
			p, err := i.ImportFrom(tt.args.path, tt.args.dir, tt.args.mode)
			if (err != nil) != tt.wantErr {
				t.Errorf("imp.ImportFrom() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err != nil {
				return
			}
			got := PackageString(p, i.Impl(p))

			// snapshot testing
			// os.MkdirAll("../testdata/"+tt.name, os.ModePerm)
			// os.WriteFile("../testdata/"+tt.name+"/types.go", []byte(got), os.ModePerm)

			if got != tt.want {
				t.Errorf("imp.ImportFrom() %v != %v", got, tt.want)
			}
		})
	}
}

func mustReadFile(name string) []byte {
	v, err := os.ReadFile(name)
	if err != nil {
		panic(err)
	}
	return v
}
