package jsonschema

import (
	"os"
	"strings"

	"github.com/valyala/fastjson"
)

func Parse(b []byte) (*fastjson.Value, error) {
	v, err := fastjson.ParseBytes(b)
	if err != nil {
		return nil, err
	}
	return parse(v), nil
}
func parse(v *fastjson.Value) *fastjson.Value {
	var visit func(k []byte, s *fastjson.Value)

	visit = func(k []byte, s *fastjson.Value) {
		if !s.Exists("x-name") {
			s.Set("x-name", fastjson.MustParse(`"`+string(k)+`"`))
		}
		s.GetObject("properties").Visit(visit)
	}

	v.GetObject("components", "schemas").Visit(visit)
	return v
}

func MustParse(b []byte) *fastjson.Value {
	v, err := Parse(b)
	if err != nil {
		panic(err)
	}
	return v
}

func MustParseFile(name string) *fastjson.Value {
	v, err := ParseFile(name)
	if err != nil {
		panic(err)
	}
	return v
}

func ParseFile(name string) (*fastjson.Value, error) {
	b, err := os.ReadFile(name)
	if err != nil {
		return nil, err
	}
	return Parse(b)
}

func Resolve(doc *fastjson.Value, value *fastjson.Value) *fastjson.Value {
	if value == nil {
		return value
	}
	if r := value.Get("$ref"); r != nil {
		return RefValue(doc, string(r.GetStringBytes()))
	} else {
		return value
	}
}

func RefValue(doc *fastjson.Value, ref string) *fastjson.Value {
	return Resolve(doc, doc.Get(RefKeys(ref)...))
}

func RefKeys(refstr string) (keys []string) {
	if refstr == "" || refstr[0:2] != "#/" {
		return
	}
	keys = strings.Split(refstr[2:], "/")
	return
}

func MergeSchemas(doc *fastjson.Value, schemas []*fastjson.Value) *fastjson.Value {
	schema := Resolve(doc, schemas[0])
	if len(schemas) > 1 {
		for _, s := range schemas[1:] {
			schema = MergeSchema(schema, Resolve(doc, s))
		}
	}
	return schema
}

func MergeSchema(dst, src *fastjson.Value) *fastjson.Value {
	src.GetObject().Visit(func(k []byte, v *fastjson.Value) {
		key := string(k)
		switch key {
		case "properties":
			src.GetObject("properties").Visit(func(k []byte, v *fastjson.Value) {
				dst.Get("properties").Set(string(k), v)
			})
		case "items":
			l := len(dst.GetArray("items"))
			for _, v := range src.GetArray("items") {
				dst.Get("items").SetArrayItem(l, v)
				l += 1
			}
		default:
			dst.Set(key, v)
		}

	})

	return dst
}

func ContainsMultipleTypes(doc *fastjson.Value, schemas []*fastjson.Value) bool {
	if len(schemas) == 0 {
		return false
	}
	t := string(Resolve(doc, schemas[0]).GetStringBytes("type"))
	for _, s := range schemas[1:] {
		st := string(Resolve(doc, s).GetStringBytes("type"))
		if t == "" && st != "" {
			t = st
			continue
		}
		if t != st {
			return true
		}
	}
	return false
}
