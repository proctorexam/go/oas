module gitlab.com/proctorexam/go/oas

go 1.19

require (
	github.com/valyala/fastjson v1.6.3
	golang.org/x/tools v0.4.0
)
